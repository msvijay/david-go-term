#!/usr/bin/perl -w
#
#
#
#

my %genemap=();
open MAPFILE,"<genename2mgiid.txt";
while(<MAPFILE>){
    chomp;
    my @line=split;
    $genemap{$line[0]}=$line[1];
}
close MAPFILE;


open FILE,"<$ARGV[0]";
my @mgilist=<FILE>;
chomp @mgilist;
close FILE;

for $i ( 0 .. $#mgilist){
    if(exists $genemap{$mgilist[$i]}) { 
	print "$mgilist[$i]\t$genemap{$mgilist[$i]}\n";
    }
}
