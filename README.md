# Register with DAVID
Goto http://david.abcc.ncifcrf.gov/webservice/register.htm and register yourself.

# Get MGI IDs for genes
```
perl get_mgiFromGenename.pl genes.txt | awk ' { print $2 } ' | sort -u > mgiids.txt
```
# Get GO term chart (BP)
```
perl david_go.pl mgiids.txt chartOutput.csv
```
